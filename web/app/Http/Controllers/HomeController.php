<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    protected $urladafruitio = 'https://io.adafruit.com/api/v2/MuhammadFarid/feeds/';
    protected $iokey = 'aio_eqeo077dmVNZejl7ZnT666rh0agk';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function getDataA() {
        $tot = 0;
        $tot2 = 0;
        $tot3 = 0;
        $url = $this->urladafruitio . 'node-1/data/last';
        $url2 = $this->urladafruitio . 'node-2/data/last';
        $url3 = $this->urladafruitio . 'node-3/data/last';
        
        $ch = curl_init(); // Initialize cURL
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_POSTFIELDS, array('X-AIO-Key' => $this->iokey));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_curl = json_decode($response, true);
        if (isset($response_curl['value'])) {
            $tot = $response_curl['value'];
        }

        $ch2 = curl_init(); // Initialize cURL
        curl_setopt($ch2, CURLOPT_URL, $url2);
        curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch2, CURLOPT_POSTFIELDS, array('X-AIO-Key' => $this->iokey));
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
        $response2 = curl_exec($ch2);
        curl_close($ch2);
        $response_curl2 = json_decode($response2, true);
        if (isset($response_curl2['value'])) {
            $tot2 = $response_curl2['value'];
        }

        $ch3 = curl_init(); // Initialize cURL
        curl_setopt($ch3, CURLOPT_URL, $url3);
        curl_setopt($ch3, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch3, CURLOPT_POSTFIELDS, array('X-AIO-Key' => $this->iokey));
        curl_setopt($ch3, CURLOPT_RETURNTRANSFER, true);
        $response3 = curl_exec($ch3);
        curl_close($ch3);
        $response_curl3 = json_decode($response3, true);
        if (isset($response_curl3['value'])) {
            $tot3 = $response_curl3['value'];
        }

        $output = array("sensor1" => $tot, "sensor2" => $tot2, "sensor3" => $tot3);

        return $output;
    }
}
