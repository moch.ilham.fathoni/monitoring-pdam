@extends('layouts.app')

@section('onHome')
active
@endsection

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Data Pemakaian Pelanggan</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Pelanggan 1</h5>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row invoice-info">
                                <div class="col-sm-6">
                                    <strong>Total Pemakaian Air</strong><br>
                                    <div id="tot-1">0 L</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Pelanggan 2</h5>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row invoice-info">
                                <div class="col-sm-6">
                                    <strong>Total Pemakaian Air</strong><br>
                                    <div id="tot-2">0 L</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Pelanggan 3</h5>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row invoice-info">
                                <div class="col-sm-6">
                                    <strong>Total Pemakaian Air</strong><br>
                                    <div id="tot-3">0 L</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
@endsection

@push('scripts')
<script>
    $(function() {
        var total = document.getElementById("tot-1");
        var total2 = document.getElementById("tot-2");
        var total3 = document.getElementById("tot-3");

        function reloadData() {
            $.ajax({
                url: "{{ route('data.a') }}",
                type: "GET",
                data: {
                    format: 'json',
                    _token:"{{ csrf_token() }}"
                },
                sasdataType: 'json',
                success: function(response){
                    total.innerHTML = response.sensor1 + " L";
                    total2.innerHTML = response.sensor2 + " L";
                    total3.innerHTML = response.sensor3 + " L";
                    console.log(response);
                }
            });
        }

        setInterval(function () {
            reloadData();
        }, 3000); // update data di web setiap 5 detik
    });
</script>
@endpush
