#include <WiFi.h>
#include <esp_now.h>
#include "Adafruit_MQTT.h"
#include "Adafruit_MQTT_Client.h"
#define PULSE_PIN 15

unsigned long previousMillis;
int interval = 3000; // 2 second

volatile long pulseCount = 0;
float calibrationFactor = 4.5;
float flowRate;
float flowLitres;
static float totalLitres = 0;
uint32_t x = 0;

volatile boolean getData = false;

// Structure example to receive data
// Must match the sender structure
typedef struct struct_message {
  int id;
  float value;
} struct_message;

// Create a struct_message called myData
struct_message myData;

/************************* WiFi Access Point *********************************/
#define WLAN_SSID       "farid123"
#define WLAN_PASS       "12345678"

/************************* Adafruit.io Setup *********************************/
#define AIO_SERVER      "io.adafruit.com"
#define AIO_SERVERPORT  1883                   // use 8883 for SSL
#define AIO_USERNAME    "MuhammadFarid"
#define AIO_KEY         "aio_eqeo077dmVNZejl7ZnT666rh0agk"

/************ Global State (you don't need to change this!) ******************/
// Create an ESP8266 WiFiClient class to connect to the MQTT server.
WiFiClient client;
// or... use WiFiFlientSecure for SSL
//WiFiClientSecure client;

// Setup the MQTT client class by passing in the WiFi client and MQTT server and login details.
Adafruit_MQTT_Client mqtt(&client, AIO_SERVER, AIO_SERVERPORT, AIO_USERNAME, AIO_KEY);

/****************************** Feeds ***************************************/
// Setup a feed called 'flow' for publishing.
// Notice MQTT paths for AIO follow the form: <username>/feeds/<feedname>
Adafruit_MQTT_Publish flow = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/node-1");
Adafruit_MQTT_Publish flow2 = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/node-2");
Adafruit_MQTT_Publish flow3 = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/node-3");

void MQTT_connect();

void ICACHE_RAM_ATTR pulseCounter()
{
  pulseCount++;
}

void setup() {
  Serial.begin(115200);
  delay(10);

  // define value
  pulseCount      = 0;
  flowRate        = 0.0;
  flowLitres      = 0.0;
  previousMillis  = 0;

  pinMode(PULSE_PIN, INPUT);
  attachInterrupt(PULSE_PIN, pulseCounter, FALLING);

  Serial.print("ESP Board MAC Address:  ");
  Serial.println(WiFi.macAddress());

  WiFi.mode(WIFI_AP);
  initEspNow();
}

void loop() {
  if((millis() - previousMillis) > interval) {
    wifiConnect();
    MQTT_connect();
    waterFlowInfo();
    mqtt.disconnect();
  }
  if (getData) {
    getData = false;
    wifiConnect();
    MQTT_connect();
    sendReceivedData();
    mqtt.disconnect();
    delay(200);
    ESP.restart();
  }
}

void initEspNow() {
  WiFi.mode(WIFI_STA);

  // Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }
  
  // Once ESPNow is successfully Init, we will register for recv CB to
  // get recv packer info
  esp_now_register_recv_cb(OnDataRecv);
}

void wifiConnect() {
  WiFi.mode(WIFI_STA);
  // Connect to WiFi access point.
  Serial.print("Connecting to ");
  Serial.println(WLAN_SSID);

  WiFi.begin(WLAN_SSID, WLAN_PASS);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println();

  Serial.println("WiFi connected");
  Serial.println("IP address: "); 
  Serial.println(WiFi.localIP());
  Serial.println();
}

// Function to connect and reconnect as necessary to the MQTT server.
// Should be called in the loop function and it will take care if connecting.
void MQTT_connect() {
  int8_t ret;

  // Stop if already connected.
  if (mqtt.connected()) {
    return;
  }

  Serial.print("Connecting to MQTT... ");

  uint8_t retries = 3;
  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
       Serial.println(mqtt.connectErrorString(ret));
       Serial.println("Retrying MQTT connection in 5 seconds...");
       mqtt.disconnect();
       delay(5000);  // wait 5 seconds
       retries--;
       if (retries == 0) {
         // basically die and wait for WDT to reset me
         while (1);
       }
  }
  Serial.println("MQTT Connected!");
}

void waterFlowInfo() {
  detachInterrupt(PULSE_PIN);
  
  flowRate = ((1000.0 / (millis() - previousMillis)) * pulseCount) / calibrationFactor;
  previousMillis = millis();
  flowLitres = (flowRate / 60);     
  totalLitres += flowLitres;
  unsigned int frac;
//  Serial.print("flowrate: ");
//  Serial.print(int(flowRate));  // Print the integer part of the variable
//  Serial.print(".");             // Print the decimal point
  frac = (flowRate - int(flowRate)) * 10;
//  Serial.print(frac, DEC) ;      // Print the fractional part of the variable
//  Serial.print("L/min");
//  Serial.print("  Current Liquid Flowing: ");             // Output separator
//  Serial.print(flowLitres);
//  Serial.print("L / 2 Sec");
//  Serial.print("  Output Liquid Quantity: ");             // Output separator
//  Serial.print(totalLitres);
//  Serial.print("L");

  pulseCount = 0;
  attachInterrupt(PULSE_PIN, pulseCounter, FALLING);
  pubData(String(totalLitres));
}

void pubData(String nilai) {
  String payload = "{\"value\":\"" + nilai;
//  payload += "\",\"lat\":\"" + latitude;
//  payload += "\",\"lon\":\"" + longitude;
  payload += "\"}";
  
  Serial.print("Sending payload: ");
  Serial.println(payload);

  if (flow.publish((char*) payload.c_str())) {
      Serial.println("Publish data ke broker node 1 berhasil");
  } else {
      Serial.println("Publish failed");
  }
}

// callback function that will be executed when data is received
void OnDataRecv(const uint8_t * mac, const uint8_t *incomingData, int len) {
  getData = true;
  memcpy(&myData, incomingData, sizeof(myData));
//  Serial.print("Bytes received: ");
//  Serial.println(len);
//  Serial.println("Berhasil menerima data");
//  Serial.print("ID: ");
//  Serial.println(myData.id);
//  Serial.print("Total pemakaian: ");
//  Serial.println(myData.value);
//  Serial.println();
}

void sendReceivedData() {
  String payload = "{\"value\":\"" + String(myData.value);
  payload += "\"}";

  Serial.print("Sending payload: ");
  Serial.println(payload);

  if (myData.id == 2) {
    if (flow2.publish((char*) payload.c_str())) {
      Serial.println("Publish data ke broker node 2 berhasil");
    } else {
      Serial.println("Publish failed");
    }
  }
  if (myData.id == 3) {
    if (flow3.publish((char*) payload.c_str())) {
      Serial.println("Publish data ke broker node 3 berhasil");
    } else {
      Serial.println("Publish failed");
    }
  }
}
