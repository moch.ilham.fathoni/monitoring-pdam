#include <WiFi.h>
#include <esp_now.h>
#define PULSE_PIN 15

unsigned long previousMillis;
int interval = 7000; // 7 second

volatile long pulseCount = 0;
float calibrationFactor = 4.5;
float flowRate;
float flowLitres;
float totalLitres;
uint32_t x = 0;

uint8_t endNode[] = {0x7C, 0x9E, 0xBD, 0xF6, 0x14, 0x48};

typedef struct struct_message {
  int id;
  float value;
} struct_message;

// Create a struct_message called myData
struct_message myData;

void ICACHE_RAM_ATTR pulseCounter()
{
  pulseCount++;
}

void setup() {
  Serial.begin(115200);
  delay(10);

  // define value
  pulseCount      = 0;
  flowRate        = 0.0;
  flowLitres      = 0.0;
  previousMillis  = 0;

  pinMode(PULSE_PIN, INPUT);
  attachInterrupt(PULSE_PIN, pulseCounter, FALLING);

  Serial.print("ESP Board MAC Address:  ");
  Serial.println(WiFi.macAddress());

  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);

  // Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  // Once ESPNow is successfully Init, we will register for Send CB to
  // get the status of Trasnmitted packet
  esp_now_register_send_cb(OnDataSent);
  
  // Register peer
  esp_now_peer_info_t peerInfo;
  memcpy(peerInfo.peer_addr, endNode, 6);
  peerInfo.channel = 0;  
  peerInfo.encrypt = false;
  
  // Add peer        
  if (esp_now_add_peer(&peerInfo) != ESP_OK){
    Serial.println("Failed to add peer");
    return;
  }
}

void loop() {
  if((millis() - previousMillis) > interval) {
    waterFlowInfo();
  }
}

void waterFlowInfo() {
  detachInterrupt(PULSE_PIN);
  
  flowRate = ((1000.0 / (millis() - previousMillis)) * pulseCount) / calibrationFactor;
  previousMillis = millis();
  flowLitres = (flowRate / 60);     
  totalLitres += flowLitres;
  unsigned int frac;
  //Serial.print("flowrate: ");
  //Serial.print(int(flowRate));  // Print the integer part of the variable
  //Serial.print(".");             // Print the decimal point
  frac = (flowRate - int(flowRate)) * 10;
  //Serial.print(frac, DEC) ;      // Print the fractional part of the variable
  //Serial.print("L/min");
  //Serial.print("  Current Liquid Flowing: ");             // Output separator
  //Serial.print(flowLitres);
  //Serial.print("L / 2 Sec");
  //Serial.print("  Output Liquid Quantity: ");             // Output separator
  //Serial.print(totalLitres);
  //Serial.println("L");

  pulseCount = 0;
  attachInterrupt(PULSE_PIN, pulseCounter, FALLING);
  // Send message via ESP-NOW
  myData.id = 2;
  myData.value = totalLitres;
  esp_err_t result = esp_now_send(endNode, (uint8_t *) &myData, sizeof(myData));
   
  if (result == ESP_OK) {
    Serial.println("Berhasil mengirim data ke gateway");
    Serial.print("ID: ");
    Serial.print(myData.id);
    Serial.print(",  Jumlah debit air: ");
    Serial.print(myData.value);
    Serial.println(" L");
  }
  else {
    Serial.println("Gagal mengirim data ke gateway");
  }
}

void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {
  Serial.print("\r\nStatus Pengiriman Paket Terakhir:\t");
  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Berhasil Terkirim" : "Gagal Terkirim");
}
